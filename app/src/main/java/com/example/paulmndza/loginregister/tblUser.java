package com.example.paulmndza.loginregister;

/**
 * Created by paulmndza on 11/16/16.
 */

public class tblUser {
    String username,email,password;

    //set value to username
    public void setUsername(String username){
        this.username = username;
    }

    //get value for username
    public String getUsername(){
        return this.username;
    }

    //get values to password
    public void setPassword(String password){
        this.password = password;
    }

    //set values for password
    public String getPassword(){
        return this.password;
    }

    //set values to email
    public void setEmail(String email){
        this.email = email;
    }

    //get values for email
    public String getEmail(){
        return this.email;
    }
}
