package com.example.paulmndza.loginregister;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by paulmndza on 11/15/16.
 */

public class display extends Activity {

    DatabaseHelper helper = new DatabaseHelper(this);

    @BindView(R.id.userList)
    ListView userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);
        ButterKnife.bind(this);
    }

}
