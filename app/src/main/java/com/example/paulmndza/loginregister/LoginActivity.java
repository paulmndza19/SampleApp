package com.example.paulmndza.loginregister;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.TabHost;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.ButterKnife;


public class LoginActivity extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);
    //Bind TextViews
    @BindView(R.id.lgnUsername)
    EditText lgnUsername;
    @BindView(R.id.lgnPassword)
    EditText  lgnPassword;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.conf_password)
    EditText confpassword;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TabHost tabHost;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        TabHost host = (TabHost)findViewById(R.id.mainTab);
        host.setup();

        //Tab 1
        TabHost.TabSpec lgn = host.newTabSpec("Login");
        lgn.setContent(R.id.login);
        lgn.setIndicator("Login");
        host.addTab(lgn);

        //Tab 2
        TabHost.TabSpec rgstr = host.newTabSpec("Register");
        rgstr.setContent(R.id.register);
        rgstr.setIndicator("Register");
        host.addTab(rgstr);




    }

    @OnClick(R.id.btnLogin)
    public void loggingin(View v){

        //EditText txtusername = (EditText)findViewById(R.id.lgnUsername);
        //EditText txtpassword = (EditText)findViewById(R.id.lgnPassword);

        String user = lgnUsername.getText().toString();
        String pass = lgnPassword.getText().toString();
        String message = "";
        String tblname = "";

        String result = helper.searchPassword(user, pass);


            if(result.equals("Logged In!")){
                Intent i =new Intent(LoginActivity.this, display.class);
                i.putExtra("Logged in as ", user + "!");
                startActivity(i);
                message = "Logged In!";
            } else if (!result.equals("Logged In!")){
                message = "Invalid Credentials";
            }

            if(user.equals("")) {
                message = "Username is Required";
            } else if(pass.equals("")) {
                message = "Password is Required";
            }
            Toast notif = Toast.makeText(LoginActivity.this, helper.getData(tblname), Toast.LENGTH_LONG);
            notif.show();
            //Toast.makeText(LoginActivity.this, helper.getData(), Toast.LENGTH_LONG);
    }

    @OnClick(R.id.btnRegister)
    public void onBtnRegister(View v){
        if (v.getId() == R.id.btnRegister){

            //      Get the values of the textfields
            EditText txtusername = (EditText)findViewById(R.id.username);
            EditText txtemail = (EditText)findViewById(R.id.email);
            EditText txtpassword = (EditText)findViewById(R.id.password);
            EditText txtconfpassword = (EditText)findViewById(R.id.conf_password);

            //      Convert values of textfields to string
            String regusername = username.getText().toString();
            String regemail = email.getText().toString();
            String regpassword = password.getText().toString();
            String regconfpassword = confpassword.getText().toString();
            String message = "";

            //      Check if Password Confirmation is successful
            if (!regpassword.equals(regconfpassword)){
                message = "Password does not match";
            }  else if(regusername.equals("")) {
                message = "Username is Required";
            } else if(regpassword.equals("")) {
                message = "Password is Required";
            } else if(regemail.equals("")) {
                message = "Email is Required";
            } else {
                //      Create User
                message = "Your account has been registered!";
                tblUser x = new tblUser();
                x.setUsername(regusername);
                x.setEmail(regemail);
                x.setPassword(regpassword);
                helper.createUser(x);

                Intent i =new Intent(LoginActivity.this, display.class);
                i.putExtra("Logged in as ", regusername + "!");
                startActivity(i);
            }
            Toast notif = Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG);
            notif.show();
        }
    }
}
