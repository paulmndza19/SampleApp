package com.example.paulmndza.loginregister;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;

/**
 * Created by paulmndza on 11/16/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sample.db";
    private static final String TABLE_NAME ="users";
    private static final String USER_ID = "id";
    private static final String USER_USERNAME = "username";
    private static final String USER_EMAIL = "email";
    private static final String USER_PASSWORD = "password";
    SQLiteDatabase db;
    private static final String TABLE_CREATE = "CREATE TABLE users (" +
                                                "id INTEGER PRIMARY KEY NOT NULL UNIQUE," +
                                                "username VARCHAR NOT NULL UNIQUE," +
                                                "email VARCHAR NOT NULL UNIQUE," +
                                                "password VARCHAR NOT NULL," +
                                                "date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
                                                "date_updated TIMESTAMP, " +
                                                "date_deleted TIMESTAMP); ";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(TABLE_CREATE);
        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        String query = "DROP TABLE IF EXISTS users";
        db.execSQL(query);
        this.onCreate(db);
    }

    public void createUser(tblUser x){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_USERNAME, x.getUsername());
        values.put(USER_EMAIL, x.getEmail());
        values.put(USER_PASSWORD, x.getPassword());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public String searchPassword(String username, String password) {
        db = this.getReadableDatabase();
        String query = "SELECT username, password FROM users WHERE username = '" + username + "' AND password = '" + password + "'";
        Cursor cursor = db.rawQuery(query, null);
        String result;
        if (cursor.getCount() == 1){
            result = "Logged In!";
        } else {
            result = "Invalid Credentials";
        }
        return result;

    }
    /*
    public void listUser(){
        db = this.getReadableDatabase();
        String selUser = "SELECT * FROM USERS;";
        Cursor userCursor = db.rawQuery(selUser, null);
        do {

        } while ()
    }
    */

    public String getData(String tblname){
        db = this.getReadableDatabase();
        String select = "SELECT username, email FROM" + tblname + ";";
        Cursor cursor = db.rawQuery(select, null);
        cursor.moveToFirst();
        //String data = cursor.getString(cursor.getColumnIndex("email"));
        String data = "shit";
        return data;
    }

}